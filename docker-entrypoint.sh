#!/usr/bin/env bash
set -e

npm ci
npx ng serve > /dev/null 2>&1 &

# first arg is `-f` or `--some-option`
# or there are no args
if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
	exec bash "$@"
fi

exec "$@"
