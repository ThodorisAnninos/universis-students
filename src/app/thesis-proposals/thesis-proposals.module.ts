import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThesisProposalsRoutingModule } from './thesis-proposals-routing.module';
import { ThesisProposalsListComponent } from './thesis-proposals-list/thesis-proposals-list.component';
import { ThesisProposalComponent } from './thesis-proposal/thesis-proposal.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { AdvancedFormsModule } from '@universis/forms';
import { NgPipesModule } from 'ngx-pipes';
import { ElementsModule } from '../elements/elements.module';
import { NgxDropzoneModule } from 'ngx-dropzone';


import * as el from "./i18n/theses.el.json";
import * as en from "./i18n/theses.en.json";

@NgModule({
  declarations: [
    ThesisProposalsListComponent,
    ThesisProposalComponent
  ],
  imports: [
    CommonModule,
    ThesisProposalsRoutingModule,
    CommonModule,
    NgPipesModule,
    SharedModule,
    ElementsModule,
    TranslateModule,
    FormsModule,
    NgxDropzoneModule,
    AdvancedFormsModule
  ]
})
export class ThesisProposalsModule {

  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }

  // tslint:disable-next-line: use-life-cycle-interface
  async ngOnInit() {
  
  }

}
