import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesisProposalsListComponent } from './thesis-proposals-list.component';

describe('ThesisProposalsListComponent', () => {
  let component: ThesisProposalsListComponent;
  let fixture: ComponentFixture<ThesisProposalsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThesisProposalsListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThesisProposalsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
