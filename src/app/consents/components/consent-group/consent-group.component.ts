import { Component, OnInit, ViewChild } from '@angular/core';
import { ErrorService, LoadingService } from '@universis/common';
import { ProfileService } from '../../../profile/services/profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { param } from 'jquery';
import { NgxConsentComponent } from '@universis/ngx-consents';

@Component({
  selector: 'app-consent-group',
  templateUrl: './consent-group.component.html',
  styleUrls: ['../../../profile/components/profile-preview/profile-preview.component.scss']
})
export class ConsentGroupComponent implements OnInit {

  public subscription?;
  public loading = true;
  public consentsGroup = null;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _location: Location,
    private _errorService: ErrorService,
    private loadingService: LoadingService) { }

  async ngOnInit() {
    this.loadingService.showLoading();

    this.subscription = this._activatedRoute.params.subscribe(params => {
      const paramsGroup = params['group'];
      if (!paramsGroup) {
        this.consentsGroup = null;
      } else if (paramsGroup) {
        this.consentsGroup = paramsGroup;
      }
      this.loading = false;
      this.loadingService.hideLoading();
    }, err => {
      this.loading = false;
      this.loadingService.hideLoading();
      console.error(err);
      this._errorService.showError(err);
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async onSubmission($event: any) {
    this._location.back();
  }

}
